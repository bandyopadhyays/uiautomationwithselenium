package com.ap.uiAutomation.factory;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;

import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DriverFactory {

	private static final Supplier<WebDriver> CHROMESUPPLIER = () -> {
		WebDriverManager.chromedriver().setup();
		return new ChromeDriver();
	};

	private static final Supplier<WebDriver> FIREFOXSUPPLIER = () -> {
		WebDriverManager.firefoxdriver().setup();
		return new FirefoxDriver();
	};

	private static final Function<WebDriver,WebDriverWait> WEBDRIVERWAIT = (driver) -> {
		return new WebDriverWait(driver, 30);
	};
	
	private static final Map<String, Supplier<WebDriver>> MAP = new HashMap<>();

	static {
		MAP.put("CHROME", CHROMESUPPLIER);
		MAP.put("FIREFOX", FIREFOXSUPPLIER);
	}
	
	public static WebDriver getDriver(String browser) {
		return MAP.get(browser).get();
	}
	
	public static WebDriverWait getWebDriverWait(WebDriver driver) {
		WebDriverWait wait = WEBDRIVERWAIT.apply(driver);
		wait.pollingEvery(Duration.ofSeconds(5));
		wait.ignoring(StaleElementReferenceException.class);
		wait.ignoring(NoSuchElementException.class);
		wait.ignoring(ElementNotVisibleException.class);
		wait.ignoring(ElementNotInteractableException.class);
		return wait;
	}
}
