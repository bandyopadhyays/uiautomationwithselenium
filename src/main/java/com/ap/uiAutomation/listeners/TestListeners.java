package com.ap.uiAutomation.listeners;

import org.apache.log4j.Logger;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.ap.uiAutomation.utils.LoggerUtils;

public class TestListeners implements ITestListener  {
	
	Logger log = LoggerUtils.getLogger(TestListeners.class);
	
	@Override	
	public void onTestStart(ITestResult result) {
	    System.out.println("before starting ..");
	    log.info("Starting Test");
	  }
	
	@Override
	public void onTestSuccess(ITestResult result) {
	    System.out.println("Test passed");
	  }
	
	@Override
	public void onTestFailure(ITestResult result) {
		System.out.println("Test Failed..");
		log.info("Test failed...");
		
	  }

}
