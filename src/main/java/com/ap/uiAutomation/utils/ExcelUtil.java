package com.ap.uiAutomation.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtil {

	private static File f = null;
	private static FileInputStream fis = null;
	private static XSSFWorkbook wb = null;

	static {
		try {
			f = new File("TestData.xlsx");
			fis = new FileInputStream(f);
			wb = new XSSFWorkbook(fis);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String getExcelCellData(String sheetName, String columnName, int rowIndex) {

		XSSFSheet sheet = null;
		XSSFRow row = null;
		XSSFCell cell = null;
		String data = "";

		sheet = wb.getSheet(sheetName);
		row = sheet.getRow(0);
		int columnCount = row.getLastCellNum();
		int requiredColIndex = 0;

		for (int i = 0; i < columnCount; i++) {
			cell = row.getCell(i);
			if (cell.getStringCellValue().equalsIgnoreCase(columnName)) {
				requiredColIndex = i;
				break;
			}
		}

		row = sheet.getRow(rowIndex);
		cell = row.getCell(requiredColIndex);

		data = cell.getStringCellValue();

		return data;

	}

	public static Object[][] getTestData(String filePath, String sheetName) {

		Object[][] dataObject = null;

		XSSFSheet sheet = null;
		XSSFRow row = null;
		XSSFCell cell = null;
		int columnCount = 0;
		int rowCount = 0;

		sheet = wb.getSheet(sheetName);
		row = sheet.getRow(0);
		columnCount = row.getLastCellNum();
		rowCount = sheet.getLastRowNum() + 1;
		System.out.println("Row=====" + rowCount);
		dataObject = new Object[rowCount - 1][columnCount];

		for (int i = 1; i < rowCount; i++) {
			row = sheet.getRow(i);
			for (int j = 0; j < columnCount; j++) {
				cell = row.getCell(j);
				dataObject[i - 1][j] = cell.getStringCellValue();
			}

		}

		return dataObject;

	}

	public static void writeValueInExcel(String sheetName, String columName, int rowIndex, String value) {
		
		XSSFSheet sheet = null;
		XSSFRow row = null;
		XSSFCell cell = null;

		sheet = wb.getSheet(sheetName);
		row = sheet.getRow(0);
		int columnCount = row.getLastCellNum();
		int targetColumnIndex = 0;
		for (int i = 0; i < columnCount; i++) {
			cell = row.getCell(i);
			if (cell.getStringCellValue().equalsIgnoreCase(columName)) {
				targetColumnIndex = i;
				break;
			}
		}

		row = sheet.getRow(rowIndex);
		cell = row.createCell(targetColumnIndex);
		cell.setCellValue(value);
		try {
			FileOutputStream fos = new FileOutputStream(f);
			wb.write(fos);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

//	public static void main(String[] args) {
//		writeValueInExcel("TC Status", "P/F", 1, "Pass");
//	}

}
