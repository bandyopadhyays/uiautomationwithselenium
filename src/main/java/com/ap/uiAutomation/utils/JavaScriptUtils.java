package com.ap.uiAutomation.utils;

import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class JavaScriptUtils {

	private static final Logger log = LoggerUtils.getLogger(JavaScriptUtils.class);

	private static void executeJavaScript(String script, WebDriver driver) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		log.info("Executing Script - " + script);
		js.executeScript(script);
	}

	private static void executeJavaScript(String script, WebDriver driver, Object... args) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		log.info("Executing Script - " + script);
		js.executeScript(script, args);
	}
	
	/**
	 * This method will scroll till web element
	 * 
	 * @param driver
	 * @param element
	 */

	public static void scrollIntoView(WebDriver driver, WebElement element) {
		String script = "arguments[0].scrollIntoView()";
		log.info("Scroll till element - " + element.toString());
		executeJavaScript(script,driver,element);
	}
	
	/**
	 * This method will scroll to web element and click
	 * 
	 * @param element
	 * @param driver
	 */
	
	public static void scrollIntoViewAndClick(WebElement element, WebDriver driver) {
		scrollIntoView(driver, element);
		log.info("Clicking on element - " + element.toString());
		element.click();
		
	}
	
	/**
	 * This method will zoom screen by 100%
	 */
	public static void zoomInBy100Percentage(WebDriver driver){
		String script = "document.body.style.zoom='100%'";
		executeJavaScript(script,driver);
	}
	
	/**
	 * This method will click on web element
	 * 
	 * @param driver
	 * @param element
	 */
	
	public static void clickElement(WebDriver driver, WebElement element) {
		String script = "arguments[0].click;";
		log.info("Clicking on element - " + element.toString());
		executeJavaScript(script, driver);
	}

}
