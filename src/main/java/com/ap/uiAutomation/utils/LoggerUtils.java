package com.ap.uiAutomation.utils;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class LoggerUtils {

	private static boolean root = false;
	
	/**
	 * This method will return logger instance of given class reference. 
	 * 
	 * @param clz
	 * @return logger instance
	 */

	public static Logger getLogger(Class<?> clz) {
		if (root) {
			return Logger.getLogger(clz);
		}

		PropertyConfigurator
				.configure(System.getProperty("user.dir") + "/src/main/resources/configFile/log4j.properties");
		root = true;
		return Logger.getLogger(clz);
	}

}
