package com.ap.uiAutomation.base;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import com.ap.uiAutomation.factory.DriverFactory;
import com.ap.uiAutomation.utils.LoggerUtils;

public class TestBase {

	protected static WebDriver driver;
	protected static WebDriverWait wait;
	
	private Logger log = LoggerUtils.getLogger(TestBase.class);

	public TestBase() {
		super();		  
	}
	
	@BeforeMethod
	@Parameters("browser")
	public void setUp(String browser) {
		log.info("Setting Browser Value --->" + browser);
		driver = DriverFactory.getDriver(browser.toUpperCase());
		log.info("Maximizing Browser...");
		driver.manage().window().maximize();
		wait = DriverFactory.getWebDriverWait(driver);
	}
	@AfterMethod
	public void tearDown() {
		log.info("Closing Browser..");
		driver.close();
		driver.quit();
	}

}
