package com.ap.uiAutomation;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ap.uiAutomation.base.TestBase;
import com.ap.uiAutomation.utils.ExcelUtil;

public class TestRun extends TestBase {
	
//	@Test
//	public void test() {
//		System.out.println("test");
//		Assert.assertTrue(false);
//	}
//	
//	@Test
//	public void test1() {
//		System.out.println("test2");
//		Assert.assertTrue(false);
//	}
	
//	@AfterMethod
//	public void after(ITestResult resut) {
//		if(resut.FAILURE == 2) {
//			System.out.println("failed");
//		} else if(resut.SUCCESS == 1) {
//			System.out.println("passed");
//		}
//	}
	
	@Test(dataProvider = "dp")
	public void login(String user, String pwd, String role) {
		System.out.println("User Name - " + user);
		System.out.println("Password - " + pwd);
		System.out.println("Role - " + role);
	}
	
	@DataProvider(name = "dataProvider")
	public Object[][] getData() {
		Object[][] data = {
				{ "user1", "pwd1" },
				{ "user2", "pwd2"},
				{ "user3", "pwd3"},
		};
		
		return data;
	}
	
	@DataProvider(name = "dp")
	public Object[][] getD() {
		 return ExcelUtil.getTestData("TestData.xlsx", "Login Details");
	}

}
